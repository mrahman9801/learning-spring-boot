package com.muhammad.springboot.learningspringboot;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Annotation to build REST API
@RestController
public class CourseController {

	// Map a URL 
	@RequestMapping("/courses-hardcoded")
	public List<Course> retrieveAllCourses() {
		// Bean comes out as JSON
		return Arrays.asList(
				new Course(1, "Learn AWS", "Muhammad"),
				new Course(2, "Learn Python", "Muhammad"),
				new Course(3, "Learn Django", "Muhammad"),
				new Course(4, "Learn FastAPI", "Muhammad"),
				new Course(5, "Learn React.js Library", "Muhammad")
				);
	}
}
