package com.muhammad.springboot.learningspringboot;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

// Annot. used for mapping a table with JPA
@Entity 
public class Course {

	@Id
	private long id;
	
	// these names are optional because 
	// the names already match with the
	// existing data that we manually hard
	// SQL wrote. .
	@Column(name="name")
	private String name;
	
	@Column(name="teacher")
	private String teacher;

	public Course() {

	}

	public Course(long id, String name, String teacher) {
		super();
		this.id = id;
		this.name = name;
		this.teacher = teacher;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getTeacher() {
		return teacher;
	}

	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", teacher=" + teacher + "]";
	}

}
