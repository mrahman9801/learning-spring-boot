package com.muhammad.springboot.learningspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// With Spring Boot, we get an embedded TomCat Server,
// logging and error handling, actuator access for prod 
// metrics monitoring, and profiles with different configurations.

@SpringBootApplication
public class LearningSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningSpringBootApplication.class, args);
	}

}
