package com.muhammad.springboot.learningspringboot.coursedatabase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.muhammad.springboot.learningspringboot.Course;
import com.muhammad.springboot.learningspringboot.coursedatabase.springdatajpa.CourseSpringDataJPARepository;

@Component
public class CourseCommandLineRunner implements CommandLineRunner {

//	@Autowired
//	private CourseJDBCRepository repository;
	
//	@Autowired
//	private CourseJPARepository repository;
	
	@Autowired
	private CourseSpringDataJPARepository repository;
	
	@Override
	public void run(String... args) throws Exception {
//		repository.insert(new Course(1, "Learn React.js", "Muhammad"));
//		repository.insert(new Course(2, "Build with Django", "Muhammad"));
//		repository.insert(new Course(3, "Design with Figma", "Muhammad"));
		
		// the interface has built-in methods
		repository.save(new Course(1, "Design with Bootstrap", "Muhammad"));
		repository.save(new Course(2, "Build with Spring Boot", "Muhammad"));
		repository.save(new Course(3, "Plan with Excalidraw", "Muhammad"));
		repository.save(new Course(4, "Build with Express.js", "Steve"));
		repository.save(new Course(5, "Plan with Notion", "Steve"));

		// we have to put the data type's initial next to the ide so 2l (2 long)
		repository.deleteById(2l);
//		System.out.println(repository.findById(3l));
//		
//		System.out.println(repository.findAll());
		
		System.out.println(repository.findByTeacher("Steve"));
	}
	
}
