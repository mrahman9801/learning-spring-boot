package com.muhammad.springboot.learningspringboot.coursedatabase.springdatajpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhammad.springboot.learningspringboot.Course;

// EVEN SIMPLER THAN REGULAR JPA OMG!!!
// literally thats all.............
public interface CourseSpringDataJPARepository extends JpaRepository<Course, Long>{
	
	// making new methods specific to your data
	List<Course> findByTeacher(String teacher);
}
