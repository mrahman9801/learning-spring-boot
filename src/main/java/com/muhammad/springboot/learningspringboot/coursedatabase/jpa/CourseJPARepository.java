package com.muhammad.springboot.learningspringboot.coursedatabase.jpa;

import org.springframework.stereotype.Repository;

import com.muhammad.springboot.learningspringboot.Course;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

@Repository
@Transactional
public class CourseJPARepository {
	
	// Compare this with JDBC... so much better.
	
	@PersistenceContext //better than Autowired for this case
	private EntityManager entityManager;
	
	public void insert(Course course) {
		entityManager.merge(course);
	}
	
	public Course findById(long id) {
		return entityManager.find(Course.class, id);
	}
	
	public void deleteById(long id) {
		Course course = entityManager.find(Course.class, id);
		entityManager.remove(course);
	}
}
