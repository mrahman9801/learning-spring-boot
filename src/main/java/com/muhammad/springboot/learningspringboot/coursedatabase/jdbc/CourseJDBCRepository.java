package com.muhammad.springboot.learningspringboot.coursedatabase.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.muhammad.springboot.learningspringboot.Course;

@Repository
public class CourseJDBCRepository {
	
	@Autowired
	private JdbcTemplate springJdbcTemplate;
	
	// Question marks are wildcards
	private static String INSERT_QUERY =
			"""
				insert into course(id,name,teacher)
				values(?, ?, ?);
			""";
	
	private static String DELETE_QUERY = 
			"""
				delete from course
				where id = ?
			""";
	
	private static String SELECT_QUERY = 
			"""
				select * from course
				where id = ?
			""";
			
	public void insert(Course course) {
		springJdbcTemplate.update(INSERT_QUERY, 
				course.getId(), course.getName(), course.getTeacher()
			);
	}
	
	public void deleteById(long id) {
		springJdbcTemplate.update(DELETE_QUERY, id);
	}
	
	public Course findById(long id) {
		return springJdbcTemplate.queryForObject(SELECT_QUERY, 
				new BeanPropertyRowMapper<>(Course.class), id);
	}
	
}
